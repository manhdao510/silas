<section class="slider_bar">
            <div class="slider">
                <div class="slides">
                    <div class="slide"><img src="./images/slider_bar/rectangle2236.png" alt="Slide 1" /></div>
                    <div class="slide"><img src="./images/slider_bar/2.png" alt="Slide 2" /></div>
                    <div class="slide"><img src="./images/slider_bar/3.png" alt="Slide 3" /></div>
                    <div class="slide"><img src="./images/slider_bar/4.png" alt="Slide 4" /></div>
                </div>
                <button class="nextBtn">
                    <img src="./images/slider_bar/arrow-drop-right-line.png" alt="Slide 4" />
                </button>

                <button class="prevBtn">
                    <img src="./images/slider_bar/arrow-drop-left-line.png" alt="Slide 4" />
                </button>
            </div>
        </section>