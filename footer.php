<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Screenr
 */
$defaults = array(
	'theme_location'  => 'primary',
	'menu'            => '',
	'container'       => 'ul',
	'container_class' => '',
	'container_id'    => '',
	'echo'            => false,
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
);
?>
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="footer-infomation">
        <div class="logo-footer">
            <img class="logo-footer-img" src="/wp-content/themes/silas/assets/images/logofooter.png" alt=""
        </div>
        <div class="footer-address_phone">
            <div class="footer_box">
                <img src="/wp-content/themes/silas/assets/images/solar_phone-line-duotone.svg" alt="" class="footer_phone">
                <span class="footer_box_text">0792 861 717</span>
            </div>
            <div class="footer_box">
                <img src="/wp-content/themes/silas/assets/images/solar_-duotone.svg" alt="" class="footer_address">
                <span class="footer_box_text">Kiot 11 đường 5, chợ Kỳ Bá,
                phường <br> Kỳ Bá, thành phố Thái Bình</span>
            </div>
        </div>
        <div class="footer-social">
            <a href="https://www.facebook.com/theminitheatre23?mibextid=ibOpuV" class="facebook_social">
                <img src="/wp-content/themes/silas/assets/images/logos_facebook.svg" alt="" class="facebook_social-icon">
            </a>
            <a href="#" class="gmail_social">
                <img src="/wp-content/themes/silas/assets/images/flat-color-icons_google%20(2).svg" alt="" class="gmail_social-icon">
            </a>
        </div>
    </div>
    <nav id="wt-nav" class="wt-nav navbar-expand-lg wt-nav-footer">
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarNav" aria-controls="navbarNav"
                aria-expanded="false" aria-label="Toggle navigation">
            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"/></svg>
        </button>
        <div class="collapse navbar-collapse wt-navigation" id="navbarNav">
			<?php echo do_shortcode( wp_nav_menu( $defaults ) ); ?>
        </div>
    </nav>
</footer>
<section class="copy_right"><h4 class="copy_right-text">
        Copyright by @SILAS,2023 - All right reserved
    </h4>
</section>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
