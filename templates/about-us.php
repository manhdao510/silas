<?php
/**
 * Template Name: About us
 *
 * The template for displaying template pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Screenr
 */

get_header();
?>
    <div id="content" class="site-content">
        <div id="content-inside" class="no-sidebar">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                    <div class="duong_dan_breadcrumb"><?php the_breadcrumb(); ?></div>
                    <div class="contet">
                        <div class="contet-main">
                            <div class="contet-title_reactive">
                                Câu Chuyện
                            </div>
                            <div class="contet-title">
                                Về Chúng Tôi
                            </div>
                        </div>
                    </div>
                    <div class="contet">
                        <div class="contet-main">
                            <p>Ở một góc nhỏ của thành phố, tồn tại một nơi đặt biệt, nơi mà vũ trụ và điện ảnh gặp
                                nhau, tạo nên một trải nghiệm <br> độc đáo và kỳ diệu. Đó chính là SILAS - mang đến cho
                                bạn cơ hội thả mình vào những câu chuyện phiêu lưu và khám <br> phá không gian đầy cuốn
                                hút.<br>

                                <br>Tại SILAS, chúng tôi có một tầm nhìn rất rõ ràng: Trở thành điểm đến hàng đầu cho
                                những người yêu thích điện ảnh và <br> sự riêng tư. Thêm vào đó, SILAS cung cấp một thực
                                đơn đa dạng, từ đồ uống tinh tế đến các món ngon nhẹ nhàng để <br> bạn có thể thưởng
                                thức trong suốt buổi xem phim. Chúng tôi tin rằng ngoài những thước phim xuất sắc, việc
                                tạo ra <br> những kỷ niệm và trải nghiệm cũng rất quan trọng tại đây.<br>

                                <br>SILAS không chỉ là một rạp phim mini, mà còn là ngồi nhà của những câu chuyện, nơi
                                bạn có thể tụ họp gia đình, gặp <br> gỡ bạn bè và người thân yêu, cùng nhau trải nghiệm
                                không gian và thời gian một cách cô đọng, thú vị và cảm xúc.</p>
                        </div>
                    </div>
                    <div class="filter_listing contet_mg-16">
                        <div class="about-main_images contet_mg-16"><img
                                    src="/wp-content/themes/silas/assets/images/image%208.png" alt=""</div>
                    </div>
                    <div class="contet_container-flex row">
                        <div class="contet_sologan col-6">
                            <div class="contet-title_reactive" style="text-align: left">Ý Nghĩa</div>
                            <div class="contet-title" style="justify-content: start">Thương Hiệu <span
                                        class="contet-title_active">“SILAS”</span></div>
                            <div class="contet_book-step">
                                <p>
                                    Cái Tên “SILAS” được tạo ra với tinh thần của sự tò mò và tự <br> do. Nó bộc lộ cho
                                    những tâm hồn luôn mong muốn tìm kiếm <br> những điều mới, khám phá những gì chưa
                                    từng trải nghiệm.<br>

                                    <br>Cụ thể hơn, SILAS mang ý nghĩa của sự mở cửa trí tưởng <br> tượng, sẵn sàng chìm
                                    đắm những thước phim lôi cuốn trong <br> không gian đầy càm xúc cùng với bạn bè và
                                    người thân yêu.<br>
                                </p>
                            </div>
                        </div>
                        <div class="contet_sologan-image col-6"><img
                                    src="/wp-content/themes/silas/assets/images/image%208.png" alt="">
                        </div>
                    </div>
            </div>
            <div class="contet_vision-mission">
                <div class="contet_sologan" style="text-align: center">
                    <div class="contet-title_reactive">Tầm Nhìn Và Sứ Mệnh</div>
                    <div class="contet-title">Của Chúng Tôi <span
                                class="contet-title_active">“SILAS”</span></div>
                </div>
                <div class="contet_container-flex row">
                    <div class="col-6">
                        <div class="contet_vision">
                            <img src="/wp-content/themes/silas/assets/images/Frame.svg" alt="">
                            <div class="contet-title">Tầm nhìn</div>
                            <div class="contet-title_sologan"><p>SILAS hướng đến xây dựng một thương hiệu bền vững và
                                    phát triển lâu dài, là “ngôi nhà” thân thiện, là điểm đến cho những buổi hẹn hò, gặp
                                    gỡ và cùng khách hàng tạo nên những kỷ niệm đáng nhớ.</p></div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="contet_vision">
                        <img src="/wp-content/themes/silas/assets/images/Frame%20(1).svg" alt="">
                        <div class="contet-title">Sứ Mệnh</div>
                        <div class="contet-title_sologan"><p>Chúng tôi muống mang đến trải nghiệm độc đáo và bay bổng:
                                SILAS cam kết tạo ra một trải nghiệm xem phim mới lạ, với không gian ấn tượng và riêng
                                tư, đem đến cho bạn những giây phút thăng hoa cùng với chất lượng dịch vụ chuyên nghiệp
                                nhất.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            </main>
        </div>
    </div>
    </div>
<?php get_footer(); ?>