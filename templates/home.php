<?php
/**
 * Template Name: Home Page
 *
 * The template for displaying template pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Screenr
 */

get_header();
?>

<div id="content" class="site-content">
    <div id="content-inside" class="no-sidebar">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div class="contet">
                    <div class="contet-main">
                        <div class="contet-title">PHIM HOT <span class="contet-title_active">SILAS</span> ĐỀ CỬ</div>
                        <div class="contet-sub">Rất nhiều các thể loại phim được update liên tục, hấp dẫn và lôi cuốn
                        </div>
                        <div class="contet-shaper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78" height="14" viewBox="0 0 78 14" fill="none">
                                <path d="M13.6893 6.68231V6.81262C10.0852 6.85606 7.17016 9.7652 7.11733 13.367H7.04337C6.98937 9.73703 4.02975 6.81145 0.388052 6.81145H0.378662V6.68349H0.388052C4.0638 6.68349 7.04455 3.70392 7.04455 0.0281745V0H7.08211V0.0281745C7.08211 3.68748 10.0359 6.65766 13.6893 6.68231Z" fill="url(#paint0_linear_10_2689)"/>
                                <path opacity="0.49" d="M35 6.68231V6.81262C31.3958 6.85606 28.4808 9.7652 28.428 13.367H28.354C28.3 9.73703 25.3404 6.81145 21.6987 6.81145H21.6893V6.68349H21.6987C25.3745 6.68349 28.3552 3.70392 28.3552 0.0281745V0H28.3928V0.0281745C28.3928 3.68748 31.3465 6.65766 35 6.68231Z" fill="url(#paint1_linear_10_2689)"/>
                                <path opacity="0.49" d="M56.3106 6.68231V6.81262C52.7065 6.85606 49.7915 9.7652 49.7387 13.367H49.6647C49.6107 9.73703 46.6511 6.81145 43.0094 6.81145H43V6.68349H43.0094C46.6851 6.68349 49.6659 3.70392 49.6659 0.0281745V0H49.7035V0.0281745C49.7035 3.68748 52.6572 6.65766 56.3106 6.68231Z" fill="url(#paint2_linear_10_2689)"/>
                                <path opacity="0.49" d="M77.6212 6.68231V6.81262C74.0171 6.85606 71.102 9.7652 71.0492 13.367H70.9753C70.9213 9.73703 67.9616 6.81145 64.3199 6.81145H64.3105V6.68349H64.3199C67.9957 6.68349 70.9764 3.70392 70.9764 0.0281745V0H71.014V0.0281745C71.014 3.68748 73.9677 6.65766 77.6212 6.68231Z" fill="url(#paint3_linear_10_2689)"/>
                                <defs>
                                    <linearGradient id="paint0_linear_10_2689" x1="7.03398" y1="0" x2="7.03398" y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint1_linear_10_2689" x1="28.3446" y1="0" x2="28.3446" y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint2_linear_10_2689" x1="49.6553" y1="0" x2="49.6553" y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint3_linear_10_2689" x1="70.9659" y1="0" x2="70.9659" y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                    </div>
                </div>
                    <?php get_template_part( 'partials/custom', 'content' );   ?>
 
                <div class="contet">
                    <div class="contet-main">
                        <div class="contet-title">Câu chuyện về <span class="contet-title_active">SILAS</span></div>
                        <div class="contet-shaper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78" height="14" viewBox="0 0 78 14" fill="none">
                                <path d="M13.6893 7.0495V7.17981C10.0852 7.22325 7.17016 10.1324 7.11733 13.7342H7.04337C6.98937 10.1042 4.02975 7.17863 0.388052 7.17863H0.378662V7.05067H0.388052C4.0638 7.05067 7.04455 4.07111 7.04455 0.395362V0.367188H7.08211V0.395362C7.08211 4.05467 10.0359 7.02484 13.6893 7.0495Z" fill="url(#paint0_linear_31_175)"/>
                                <path d="M35 7.0495V7.17981C31.3958 7.22325 28.4808 10.1324 28.428 13.7342H28.354C28.3 10.1042 25.3404 7.17863 21.6987 7.17863H21.6893V7.05067H21.6987C25.3745 7.05067 28.3552 4.07111 28.3552 0.395362V0.367188H28.3928V0.395362C28.3928 4.05467 31.3465 7.02484 35 7.0495Z" fill="url(#paint1_linear_31_175)"/>
                                <path opacity="0.49" d="M56.3106 7.0495V7.17981C52.7065 7.22325 49.7915 10.1324 49.7387 13.7342H49.6647C49.6107 10.1042 46.6511 7.17863 43.0094 7.17863H43V7.05067H43.0094C46.6851 7.05067 49.6659 4.07111 49.6659 0.395362V0.367188H49.7035V0.395362C49.7035 4.05467 52.6572 7.02484 56.3106 7.0495Z" fill="url(#paint2_linear_31_175)"/>
                                <path opacity="0.49" d="M77.6212 7.0495V7.17981C74.0171 7.22325 71.102 10.1324 71.0492 13.7342H70.9753C70.9213 10.1042 67.9616 7.17863 64.3199 7.17863H64.3105V7.05067H64.3199C67.9957 7.05067 70.9764 4.07111 70.9764 0.395362V0.367188H71.014V0.395362C71.014 4.05467 73.9677 7.02484 77.6212 7.0495Z" fill="url(#paint3_linear_31_175)"/>
                                <defs>
                                    <linearGradient id="paint0_linear_31_175" x1="7.03398" y1="0.367187" x2="7.03398" y2="13.7342" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint1_linear_31_175" x1="28.3446" y1="0.367187" x2="28.3446" y2="13.7342" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint2_linear_31_175" x1="49.6553" y1="0.367187" x2="49.6553" y2="13.7342" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint3_linear_31_175" x1="70.9659" y1="0.367187" x2="70.9659" y2="13.7342" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                    </div>
                </div>
                <section class="title-contact">
                    <div class="title-desription">
                        <div class="title-box">
                            <div class="title-line title-sub">
                                <h1>Về chúng tôi</h1>
                                <p>
                                    SILAS không chỉ là một rạp phim mini, mà còn là ngôi nhà của những câu chuyện, nơi
                                    bạn có
                                    thể tụ họp gia đình, gặp gỡ bạn bè và người thân yêu, cùng nhau trải nghiệm không
                                    gian và
                                    thời gian một cách cô đọng, thú vị và cảm xúc.
                                </p>
                            </div>
                            <div class="title-sub">
                                <h1>Ý nghĩa thương hiệu</h1>
                                <p>
                                    Cái Tên “SILAS” được tạo ra với tinh thần của sự tò mò và tự do. Nó bộc lộ cho những tâm hồn luôn mong muốn tìm kiếm những điều mới, khám phá những gì chưa từng trải nghiệm. <br><br>

                                    Cụ thể hơn, SILAS mang ý nghĩa của sự mở cửa trí tưởng tượng, sẵn sàng chìm đắm những thước phim lôi cuốn trong không gian đầy càm xúc cùng với bạn bè và người thân yêu.
                                </p>
                            </div>
                            <div class="title-sub">
                                <h1>Tầm Nhìn Và Sứ Mệnh</h1>
                                <p>
                                    SILAS hướng đến xây dựng một thương hiệu bền vững và phát triển lâu dài, là “ngôi nhà” thân thiện, là điểm đến cho những buổi hẹn hò, gặp gỡ và cùng khách hàng tạo nên những kỷ niệm đáng nhớ. <br><br>
                                    Chúng tôi muống mang đến trải nghiệm độc đáo và bay bổng: SILAS cam kết tạo ra một trải nghiệm xem phim mới lạ, với không gian ấn tượng và riêng tư, đem đến cho bạn những giây phút thăng hoa cùng với chất lượng dịch vụ chuyên nghiệp nhất.
                                </p>

                            </div>
                        </div>
                    </div>
                    <div class="title-img">
                        <img src="/wp-content/themes/silas/assets/images/z5119866323637_f9bf03d2cbc3740ea960c236be41b770.jpg"/>
                    </div>
                </section>
                <div class="contet">
                    <div class="contet-main">
                        <div class="contet-title">COMBO XEM PHIM TẠI <span class="contet-title_active">SILAS</span>
                        </div>
                        <div class="contet-shaper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78" height="15" viewBox="0 0 78 15" fill="none">
                                <path d="M13.6893 7.4162V7.54651C10.0852 7.58995 7.17016 10.4991 7.11733 14.1009H7.04337C6.98937 10.4709 4.02975 7.54533 0.388052 7.54533H0.378662V7.41737H0.388052C4.0638 7.41737 7.04455 4.43781 7.04455 0.762061V0.733887H7.08211V0.762061C7.08211 4.42137 10.0359 7.39154 13.6893 7.4162Z" fill="url(#paint0_linear_13_3204)"/>
                                <path d="M35 7.4162V7.54651C31.3958 7.58995 28.4808 10.4991 28.428 14.1009H28.354C28.3 10.4709 25.3404 7.54533 21.6987 7.54533H21.6893V7.41737H21.6987C25.3745 7.41737 28.3552 4.43781 28.3552 0.762061V0.733887H28.3928V0.762061C28.3928 4.42137 31.3465 7.39154 35 7.4162Z" fill="url(#paint1_linear_13_3204)"/>
                                <path d="M56.3106 7.4162V7.54651C52.7065 7.58995 49.7915 10.4991 49.7387 14.1009H49.6647C49.6107 10.4709 46.6511 7.54533 43.0094 7.54533H43V7.41737H43.0094C46.6851 7.41737 49.6659 4.43781 49.6659 0.762061V0.733887H49.7035V0.762061C49.7035 4.42137 52.6572 7.39154 56.3106 7.4162Z" fill="url(#paint2_linear_13_3204)"/>
                                <path opacity="0.49" d="M77.6212 7.4162V7.54651C74.0171 7.58995 71.102 10.4991 71.0492 14.1009H70.9753C70.9213 10.4709 67.9616 7.54533 64.3199 7.54533H64.3105V7.41737H64.3199C67.9957 7.41737 70.9764 4.43781 70.9764 0.762061V0.733887H71.014V0.762061C71.014 4.42137 73.9677 7.39154 77.6212 7.4162Z" fill="url(#paint3_linear_13_3204)"/>
                                <defs>
                                    <linearGradient id="paint0_linear_13_3204" x1="7.03398" y1="0.733887" x2="7.03398" y2="14.1009" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint1_linear_13_3204" x1="28.3446" y1="0.733887" x2="28.3446" y2="14.1009" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint2_linear_13_3204" x1="49.6553" y1="0.733887" x2="49.6553" y2="14.1009" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                    <linearGradient id="paint3_linear_13_3204" x1="70.9659" y1="0.733887" x2="70.9659" y2="14.1009" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185"/>
                                        <stop offset="1" stop-color="#F6D9BF"/>
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                    </div>
                </div>
                <section class="title-price">
                    <div class="price-img">
                        <img src="/wp-content/themes/silas/assets/images/z5165552933218_e312b9a9f5fe69eca206d756e9bc9456.jpg" />
                    </div>
                    <div class="price-img">
                        <img src="/wp-content/themes/silas/assets/images/z5165552944778_e2f201a4ed88613c44be03f9bde0a089.jpg" />
                        <div class="price-shaper-bot">
                            <svg xmlns="http://www.w3.org/2000/svg" width="47" height="57" viewBox="0 0 47 57"
                                fill="none">
                                <path d="M0 56.3672H17.8937V53.2339H0V56.3672Z" fill="url(#paint0_linear_13_3214)" />
                                <path
                                    d="M17.8937 53.2339V56.3672C33.9689 56.3672 47 43.3921 47 27.386C47 15.087 39.3052 4.57568 28.4471 0.367188V3.76764C37.5165 7.80463 43.8532 16.8713 43.8532 27.386C43.8532 41.6375 32.2067 53.2339 17.8937 53.2339Z"
                                    fill="url(#paint1_linear_13_3214)" />
                                <defs>
                                    <linearGradient id="paint0_linear_13_3214" x1="-1.5" y1="66.8667" x2="30.5"
                                        y2="4.8667" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F6D9BF" />
                                        <stop offset="0.606809" stop-color="white" />
                                        <stop offset="1" stop-color="white" stop-opacity="0" />
                                    </linearGradient>
                                    <linearGradient id="paint1_linear_13_3214" x1="-1.5" y1="66.8667" x2="30.5"
                                        y2="4.8667" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#F6D9BF" />
                                        <stop offset="0.606809" stop-color="white" />
                                        <stop offset="1" stop-color="white" stop-opacity="0" />
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                        <div class="shaper-right right-one">
                            <img src="/wp-content/themes/silas/images/shaper/Vector.png" />
                        </div>
                        <div class="shaper-right right-two">
                            <img src="/wp-content/themes/silas/images/shaper/Vector-1.png" />
                        </div>
                        <div class="shaper-right right-there">
                            <img src="/wp-content/themes/silas/images/shaper/Vector-2.png" />
                        </div>
                    </div>
                </section>
                <div class="contet">
                    <div class="contet-main">
                        <div class="contet-title">CHƯƠNG TRÌNH <span class="contet-title_active">KHUYẾN MÃI</span></div>
                        <div class="contet-sub">Rất nhiều chương trình khuyến mãi đặc biệt chỉ có tại SILAS</div>
                        <div class="contet-shaper">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"
                                fill="none">
                                <path
                                    d="M13.6893 6.68231V6.81262C10.0852 6.85606 7.17016 9.7652 7.11733 13.367H7.04337C6.98937 9.73703 4.02975 6.81145 0.388052 6.81145H0.378662V6.68349H0.388052C4.0638 6.68349 7.04455 3.70392 7.04455 0.0281745V0H7.08211V0.0281745C7.08211 3.68748 10.0359 6.65766 13.6893 6.68231Z"
                                    fill="url(#paint0_linear_10_2686)" />
                                <defs>
                                    <linearGradient id="paint0_linear_10_2686" x1="7.03398" y1="0" x2="7.03398"
                                        y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185" />
                                        <stop offset="1" stop-color="#F6D9BF" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"
                                fill="none">
                                <path
                                    d="M13.6893 6.68231V6.81262C10.0852 6.85606 7.17016 9.7652 7.11733 13.367H7.04337C6.98937 9.73703 4.02975 6.81145 0.388052 6.81145H0.378662V6.68349H0.388052C4.0638 6.68349 7.04455 3.70392 7.04455 0.0281745V0H7.08211V0.0281745C7.08211 3.68748 10.0359 6.65766 13.6893 6.68231Z"
                                    fill="url(#paint0_linear_10_2686)" />
                                <defs>
                                    <linearGradient id="paint0_linear_10_2686" x1="7.03398" y1="0" x2="7.03398"
                                        y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185" />
                                        <stop offset="1" stop-color="#F6D9BF" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"
                                fill="none">
                                <path
                                    d="M13.6893 6.68231V6.81262C10.0852 6.85606 7.17016 9.7652 7.11733 13.367H7.04337C6.98937 9.73703 4.02975 6.81145 0.388052 6.81145H0.378662V6.68349H0.388052C4.0638 6.68349 7.04455 3.70392 7.04455 0.0281745V0H7.08211V0.0281745C7.08211 3.68748 10.0359 6.65766 13.6893 6.68231Z"
                                    fill="url(#paint0_linear_10_2686)" />
                                <defs>
                                    <linearGradient id="paint0_linear_10_2686" x1="7.03398" y1="0" x2="7.03398"
                                        y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185" />
                                        <stop offset="1" stop-color="#F6D9BF" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"
                                fill="none">
                                <path
                                    d="M13.6893 6.68231V6.81262C10.0852 6.85606 7.17016 9.7652 7.11733 13.367H7.04337C6.98937 9.73703 4.02975 6.81145 0.388052 6.81145H0.378662V6.68349H0.388052C4.0638 6.68349 7.04455 3.70392 7.04455 0.0281745V0H7.08211V0.0281745C7.08211 3.68748 10.0359 6.65766 13.6893 6.68231Z"
                                    fill="url(#paint0_linear_10_2686)" />
                                <defs>
                                    <linearGradient id="paint0_linear_10_2686" x1="7.03398" y1="0" x2="7.03398"
                                        y2="13.367" gradientUnits="userSpaceOnUse">
                                        <stop stop-color="#D5A185" />
                                        <stop offset="1" stop-color="#F6D9BF" />
                                    </linearGradient>
                                </defs>
                            </svg>
                        </div>
                    </div>
                </div>
                <div class="filter_listing contet_mg-16">
                    <section class="row">
                        <?php do_action('feature_promotion_home');  ?>
                    </section>
                </div>
            </main><!-- #main -->
        </div><!-- #primary -->

    </div><!--#content-inside -->
</div><!-- #content -->

<?php get_footer(); ?>