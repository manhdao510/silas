<?php
/**
 * Template Name: Đặt phòng
 *
 * The template for displaying template pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Screenr
 */

get_header();
?>
<div id="content" class="site-content">
	<div id="content-inside" class="no-sidebar">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="duong_dan_breadcrumb"><?php the_breadcrumb(); ?></div>
				<div class="contet">
					<div class="contet-main">
						<div class="contet-title">
							<img src="/wp-content/themes/silas/assets/images/Vector.svg" class="contet-title_img">
							Đặt phòng
							<img src="/wp-content/themes/silas/assets/images/Vector.svg" class="contet-title_img">
						</div>
						<div class="contet-line"></div>
					</div>
				</div>
                <div class="filter_listing contet_mg-16">
                    <div class="contet_book-film contet_mg-16">
                        <h1><img src="/wp-content/themes/silas/assets/images/vector_title.svg"> BẠN đã sẵn
                            sàng <span class="contet-film_bold">ĐẶT PHÒNG</span> tại <span
                                    class="contet-film_bold_active">SILAS Cinema</span>hôm nay chưa</h1>
                        <div class="contet_book-step">
                        </div>
                        <div class="contet_address-box">
                            <img src="/wp-content/themes/silas/assets/images/solar_-duotone.svg" alt=""
                                 class="footer_address">
                            <span class="footer_box_text">Địa Chỉ: <h3>Kiot 11 đường 5, chợ Kỳ Bá,phường Kỳ Bá, thành phố Thái Bình</h3></span>
                        </div>
                        <div class="contet_address-box">
                            <img src="/wp-content/themes/silas/assets/images/solar_phone-line-duotone.svg"
                                 alt="" class="footer_phone">
                            <span class="footer_box_text">Đặt Phòng: <h3>0792 861 717</h3></span>
                        </div>
                        <div class="contet_address-box">
                            <img src="/wp-content/themes/silas/assets/images/logos_facebook.svg" alt=""
                                 class="footer_phone">
                            <a href="https://www.facebook.com/theminitheatre23?mibextid=ibOpuV"><span class="footer_box_text">FaceBook</span> </a>
                        </div>
                    </div>
                </div>
                <div class="filter_listing contet_mg-16">
                    <div class="contet_discount-title">
                        <div class="contet_title-main">
                            <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                            <h1>Khuyến Mại</h1>
                        </div>
                    </div>
                    <div class="contet_relate-container row">
						<?php do_action('feature_promotion');  ?>
                    </div>
                </div>
                <div class="filter_listing contet_mg-16">
                    <div class="contet_discount-title">
                        <div class="contet_title-main">
                            <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                            <h1>Phim Mới</h1>
                        </div>
                        <a href="<?php echo get_permalink(get_page_by_path('silas-cinema')); ?>" class="contet_title-more">
                            <h1>Xem Thêm</h1>
                            <img src="/wp-content/themes/silas/assets/images/arrow-right.svg" alt="">
                        </a>
                    </div>
                    <div class="contet_relate-container row">
						<?php do_action('new_movie');  ?>
                    </div>
                </div>
            </main>
		</div>
	</div>
</div>
<?php get_footer(); ?>
