<?php
/**
 * Template Name: Service Cinema
 *
 * The template for displaying template pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Screenr
 */

get_header();
?>
    <div id="content" class="site-content">
        <div id="content-inside" class="no-sidebar">
            <div id="primary" class="content-area">
                <main id="main" class="site-main" role="main">
                    <div class="duong_dan_breadcrumb"><?php the_breadcrumb(); ?></div>
                    <div class="contet">
                        <div class="contet-main">
                            <div class="contet-title">
                                <img src="/wp-content/themes/silas/assets/images/Vector.svg" class="contet-title_img">
                                Các dịch vụ Có Tại <span class="contet-title_active">SILAS</span>
                                <img src="/wp-content/themes/silas/assets/images/Vector.svg" class="contet-title_img">
                            </div>
                            <div class="contet-line"></div>
                        </div>
                    </div>
                    <div class="filter_listing">
                        <div class="contet_container-relate">
                            <div class="contet_discount-title">
                                <div class="contet_title-main">
                                    <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                                    <h1>Thực Đơn</h1>
                                </div>
                            </div>
                            <div class="contet_relate-container row">
                                <div class="contet_menu-box col-lg-3 col-sm-12 col-md-12 col-12">
                                    <img src="/wp-content/themes/silas/assets/images/z5165552933218_e312b9a9f5fe69eca206d756e9bc9456.jpg"
                                         alt="">
                                </div>
                                <div class="contet_menu-box col-lg-3 col-sm-12 col-md-12 col-12">

                                    <img src="/wp-content/themes/silas/assets/images/z5165552944778_e2f201a4ed88613c44be03f9bde0a089.jpg"
                                         alt="">
                                </div>
                                <div class="contet_menu-box col-lg-3 col-sm-12 col-md-12 col-12">

                                    <img src="/wp-content/themes/silas/assets/images/z5165552935538_9c8a820adf76df8d3f1908cc6324a704.jpg"
                                         alt="">
                                </div>
                                <div class="contet_menu-box col-lg-3 col-sm-12 col-md-12 col-12">

                                    <img src="/wp-content/themes/silas/assets/images/z5165552942668_980ee3f12d8b3482a9583d0cdec70bb5.jpg"
                                         alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter_listing contet_mg-16">
                        <div class="contet_container-relate">
                            <div class="contet_discount-title">
                                <div class="contet_title-main">
                                    <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                                    <h1>Trang Trí</h1>
                                </div>
                            </div>
                            <div class="contet_decor-container">
                                <div class="contet_decor-title">
                                    <p>
                                        Các gói trang trí sự kiện tại SILAS <br>
                                        1. PHÒNG COUPLE <br>
                                        - Gói tiết kiệm<strong> 599k</strong> đã bao gồm có: Chữ, stick gắn tường theo event, bóng bay thả nền ,dây đèn led trang trí (*Free nến điện chill) <br><br>

                                        - Gói tiêu chuẩn<strong> 1tr199k</strong> đã bao gồm có: Bóng bay thả trần, bóng bay thả nền, bóng bay trái tim gắn nền, chữ, stick gắn tường theo event, đèn led trang trí (* Free nến điện chill, tặng 02 ly rượu vang Italia)
                                        <br> <br>
                                        - Gói Vip<strong> 1tr799k</strong> đã bao gồm có: Bóng bay thả trần, bóng bay thả nền, bóng bay trái tim gắn nền, chữ, stick gắn tường theo event, đèn led trang trí, bóng Jumbo khổng lồ in theo yêu cầu (* Free nến điện chill, tặng 02 ly rượu vang Italia, tặng 01 bánh kem theo event)
                                        <br> <br>
                                        2. PHÒNG FAMILY <br>
                                        - Gói tiết kiệm <strong>799k</strong> đã bao gồm có: Chữ, stick gắn tường theo event, bóng bay thả nền, dây đèn led trang trí (* Free nến điện chill )
                                        <br><br>
                                        - Gói tiêu chuẩn <strong>1tr399k</strong> đã bao gồm có: Bóng bay thả trần, bóng bay thả nền, bóng bay trái tim gắn nền, chữ, stick gắn tường theo event, đèn led trang trí (* Free nến điện chill, tặng 04 ly rượu vang Italia )
                                        <br><br>
                                        - Gói Vip <strong>1tr999k</strong> đã bao gồm có: Bóng bay thả trần, bóng bay thả nền, bóng bay trái tim gắn nền, chữ, stick gắn tường theo event, đèn led trang trí, bóng Jumbo khổng lồ in theo yêu cầu (*Free nến điện chill, tặng 04 ly rượu vang Italia, tặng 01 bánh kem theo event )
                                    </p>
                                </div>
                                <div class="contet_decor-box row">
                                    <div class="col-12 col-lg-6"> <img src="/wp-content/themes/silas/assets/images/z5265168303458_d1e375b8bcd9e5f9f52aa0b471c1484e.jpg"></div>
                                    <div class="col-12 col-lg-6"> <img src="/wp-content/themes/silas/assets/images/z5265168303812_52217ad96618c9224962ebba7cd17155.jpg"></div>
                                    <div class="col-12 col-lg-6"> <img src="/wp-content/themes/silas/assets/images/z5265168316458_b20b28a7f5b4b4d5c3851c0be73b9d3d.jpg"></div>
                                    <div class="col-12 col-lg-6"> <img src="/wp-content/themes/silas/assets/images/z5265168320995_7ddb91b64be76ad7e7dc9bb86d2e897b.jpg"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter_listing contet_mg-16">
                        <div class="contet_discount-title">
                            <div class="contet_title-main">
                                <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                                <h1>Khuyến Mại</h1>
                            </div>
                        </div>
                        <div class="contet_relate-container row">
							<?php do_action( 'feature_promotion' ); ?>
                        </div>
                    </div>
                    <div class="filter_listing contet_mg-16">
                        <div class="contet_discount-title">
                            <div class="contet_title-main">
                                <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                                <h1>Phim Mới</h1>
                            </div>
                            <a href="<?php echo get_permalink( get_page_by_path( 'silas-cinema' ) ) ?>"
                               class="contet_title-more">
                                <h1>Xem Thêm</h1>
                                <img src="/wp-content/themes/silas/assets/images/arrow-right.svg" alt="">
                            </a>
                        </div>
                        <div class="contet_relate-container row">
							<?php do_action( 'new_movie' ); ?>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </div>
<?php get_footer();