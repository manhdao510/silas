

const sliderBox = document.querySelector(".sliderBox");
const sliderBoxImages = document.querySelectorAll(".sliderBox-img");
const images = Array.from(sliderBoxImages);
const currentPath = window.location.pathname;

jQuery(document).on('ready', function () {
    jQuery('button').on('click', function (e) {
        let show = "active";
        let grid = document.querySelector(".grid");
        let list = document.querySelector(".list");
        let listimagebtn = list.querySelector('img');
        let gridimagebtn = grid.querySelector('img');
        if (jQuery(this).hasClass('grid')) {
            jQuery('#container .slider-bar').removeClass('list').addClass('grid');
            jQuery('#container .sliderBox-img').removeClass('listBox');
            jQuery('#container .sliderBox-img').removeClass('col-lg-12').addClass('col-md-6 col-lg-4');
            grid.classList.toggle(show);
            jQuery('.list').removeClass(show);
            listimagebtn.src=('/wp-content/themes/silas/assets/images/fa-solid_list_white.svg')
            gridimagebtn.src=('/wp-content/themes/silas/assets/images/material-symbols_grid-on.svg')
            jQuery('.list').removeClass('disabled');

            jQuery('.grid.active').addClass('disabled');
        } else if (jQuery(this).hasClass('list')) {
            jQuery('#container .slider-bar').removeClass('grid').addClass('list');
            jQuery('#container .sliderBox-img').addClass('listBox');
            jQuery('#container .sliderBox-img').removeClass('col-md-6 col-lg-4').addClass('col-lg-12');
            list.classList.toggle(show);
            jQuery('.grid').removeClass(show);
            listimagebtn.src=('/wp-content/themes/silas/assets/images/fa-solid_list.svg')
            gridimagebtn.src=('/wp-content/themes/silas/assets/images/material-symbols_grid-white.svg')
            jQuery('.grid').removeClass('disabled');
            jQuery('.list.active').addClass('disabled');


        }
    });
    jQuery('li.menu-item-has-children').each( function() {
        jQuery(this).prepend('<div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div>');
    });
    jQuery(document).on('click', '#search_bar-icon', function () {
        const searchForm = document.querySelector(".search-form");
        const isVisible = "is-visible";
        searchForm.classList.toggle(isVisible);
    });
    jQuery(document).on('click', '.navbar-toggler', function () {
        const collapsform = document.querySelector("#navbarNav");
        const show = "show";
        collapsform.classList.toggle(show);
    });
    jQuery(document).on('click', '.menu-item-has-children', function () {
        const collapssubmenu = document.querySelector(".sub-menu");
        const show = "show";
        collapssubmenu.classList.toggle(show);
    });
});

document.addEventListener("DOMContentLoaded", function () {
    
    const slides = ["/wp-content/themes/silas/assets/images/bg-x1.png", "/wp-content/themes/silas/assets/images/bg-x2%20.png", "/wp-content/themes/silas/assets/images/bg-x3.png", "/wp-content/themes/silas/assets/images/bg-x4.png",];
    let currentSlide = 0;
    const slideContainer = document.querySelector(".slides");
    const prevBtn = document.querySelector(".prevBtn");
    const nextBtn = document.querySelector(".nextBtn");

    function showSlide(index) {
        slideContainer.innerHTML = `<div class="slide"><img src="${slides[index]}" alt="Slide ${index + 1}" /></div>`;
    }

    function prevSlide() {
        currentSlide = currentSlide === 0 ? slides.length - 1 : currentSlide - 1;
        showSlide(currentSlide);
    }

    function nextSlide() {
        currentSlide = currentSlide === slides.length - 1 ? 0 : currentSlide + 1;
        showSlide(currentSlide);
    }

    prevBtn.addEventListener("click", prevSlide);
    nextBtn.addEventListener("click", nextSlide);

    // Hiển thị slide đầu tiên ban đầu
    showSlide(currentSlide);
});
const slider = document.querySelector(".sliderBox");
const prevButton = document.querySelector(".prevBtn-box");
const nextButton = document.querySelector(".nextBtn-box");
const dotsContainer = document.querySelector(".dots-container");

function updateActiveDot() {
    const dots = Array.from(dotsContainer.querySelectorAll(".dot"));
    const activeIndex = images.findIndex((image) => image === slider.firstElementChild.querySelector("img").src);
    dots.forEach((dot, index) => {
        if (index === activeIndex) {
            console.log(dot);
            dot.classList.add("active");
        } else {
           
            dot.classList.remove("active");
        }
    });
}
if (currentPath === "/") {
  document.addEventListener("DOMContentLoaded", function () {
   const prevButton = document.querySelector(".prevBtn-box");
  const nextButton = document.querySelector(".nextBtn-box");
  let currentIndex = 0;
  // Hiển thị 3 slider đầu tiên khi tải trang
  showSlides(currentIndex);

  // Xử lý sự kiện nút Previous
  prevButton.addEventListener("click", () => {
    currentIndex = (currentIndex - 1 + sliderBoxImages.length) % sliderBoxImages.length;
    showSlides(currentIndex);
  });

  // Xử lý sự kiện nút Next
  nextButton.addEventListener("click", () => {
    currentIndex = (currentIndex + 1) % sliderBoxImages.length;
    showSlides(currentIndex);
  });

  // Hiển thị 3 slider tại chỉ mục currentIndex và 2 chỉ mục xung quanh nó
  function showSlides(index) {
    sliderBoxImages.forEach((img, i) => {
      if (i === index || i === (index + 1) % sliderBoxImages.length || i === (index + 2) % sliderBoxImages.length) {
        img.style.display = "block";
      } else {
        img.style.display = "none";
      }
    });
  }
}); 
function createDots() {
    for (let i = 0; i < sliderBoxImages.length; i++) {
        const dot = `<div class="dot"></div>`;
        dotsContainer.insertAdjacentHTML("beforeend", dot);
    }
}

createDots();

function hideFirstAndLastSlides() {
    const slides = Array.from(slider.querySelectorAll(".sliderBox-img"));
    slides.forEach((s) => s.classList.remove("opacity-0"));
}

prevButton.addEventListener("click", function () {
    slider.insertAdjacentElement("afterbegin", slider.lastElementChild);
    hideFirstAndLastSlides();
    updateActiveDot();
});

nextButton.addEventListener("click", function () {
    slider.insertAdjacentElement("beforeend", slider.firstElementChild);
    hideFirstAndLastSlides();
    updateActiveDot();
});


dotsContainer.addEventListener("click", function (event) {
    if (event.target.classList.contains("dot")) {
        const clickedIndex = Array.from(dotsContainer.querySelectorAll(".dot")).indexOf(event.target);
        const activeIndex = images.indexOf(slider.firstElementChild.querySelector("img").src);
        const diff = clickedIndex - activeIndex;
        if (diff > 0) {
            for (let i = 0; i < diff; i++) {
                slider.insertAdjacentElement("beforeend", slider.firstElementChild);
            }
        } else if (diff < 0) {
            for (let i = 0; i > diff; i--) {
                slider.insertAdjacentElement("afterbegin", slider.lastElementChild);
            }
        }
        hideFirstAndLastSlides();
        updateActiveDot();
    }
});

hideFirstAndLastSlides();
}



if (currentPath === "/silas-cinema/") {
document.getElementById('searchControlsName').addEventListener('change', function() {
    var selectedValue = this.value;
    var currentURL = window.location.href;
    var updatedURL = updateQueryStringParameter(currentURL, 'category', selectedValue);
    window.location.href = updatedURL;
});
document.getElementById('searchControlsSortBy').addEventListener('change', function() {
    var selectedValue = this.value;
    var currentURL = window.location.href;
    var updatedURL = updateQueryStringParameter(currentURL, 'sortBy', selectedValue);
    window.location.href = updatedURL;
});
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }
}
}