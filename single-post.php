<?php
/**
 * The template for displaying movie single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Screenr
 */

get_header(); ?>

<div id="content" class="site-content">
	<div id="content-inside" class="no-sidebar">
		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">
				<div class="duong_dan_breadcrumb"><?php the_breadcrumb(); ?></div>
				<?php while ( have_posts() ) : the_post();
					global $post;

					$post_id = get_the_ID();
					$custom_field_value = get_post_meta($post_id, 'promotion_image', true);
					$image_data = wp_get_attachment_image_src($custom_field_value, 'full');
					$image_url = ($image_data) ? $image_data[0] : '';
					$movie_time = get_post_meta($post_id, 'movie_time', true);
					$terms_category = wp_get_post_terms(get_the_ID(), 'category');
					$link_url=get_post_meta($post_id, 'link_url', true);
					if(!empty($terms_category)){
						$name_categoty= $terms_category[0]->name;
					}
					if (!empty($movie_time)) {
						list($hour, $minute, $second) = explode(":", $movie_time);
						$formattedTime = "$hour giờ $minute phút $second giây";
					} else {
						$formattedTime = 'N/A';
					}
					$point = get_post_meta($post_id, 'point', true);

					?>
					<div class="contet_container">
						<div class="contet_container-flex">
							<div class="contet_detail">
								<div class="contet_title">
									<h1 class="contet_title-text"><?php echo get_the_title(); ?></h1>
								</div>
								<div class="contet_image-film contet_mg-16">
									<img src="<?php echo $image_url ?>"
									     alt="<?php echo get_the_title(); ?>">

								</div>
								<div class="contet_description-film contet_mg-16">
									<h1><img src="/wp-content/themes/silas/assets/images/vector_title.svg"> Nội dung khuyến mãi</h1>
									<?php the_Content(); ?>
								</div>
								<div class="contet_description-film contet_mg-16">
									<h1><img src="/wp-content/themes/silas/assets/images/vector_title.svg"> BẠN đã sẵn
										sàng <span class="contet-film_bold">ĐẶT PHÒNG</span> tại <span
											class="contet-film_bold_active">SILAS Cinema</span>hôm nay chưa</h1>
									<div class="contet_address-box">
										<img src="/wp-content/themes/silas/assets/images/solar_-duotone.svg" alt=""
										     class="footer_address">
										<span class="footer_box_text">Địa Chỉ: <h3>Kiot 11 đường 5, chợ Kỳ Bá,phường Kỳ Bá, thành phố Thái Bình</h3></span>
									</div>
									<div class="contet_address-box">
										<img src="/wp-content/themes/silas/assets/images/solar_phone-line-duotone.svg"
										     alt="" class="footer_phone">
										<span class="footer_box_text">Đặt Phòng: <h3>0792 861 717</h3></span>
									</div>
									<div class="contet_address-box">
										<img src="/wp-content/themes/silas/assets/images/logos_facebook.svg" alt=""
										     class="footer_phone">
                                        <a href="https://www.facebook.com/theminitheatre23?mibextid=ibOpuV"><span class="footer_box_text">FaceBook</span> </a>
									</div>
								</div>
							</div>
							<div class="contet_discount">
								<div class="contet_discount-title">
									<div class="contet_title-main">
										<img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
										<h1>Khuyến Mại</h1>
									</div>
								</div>
                                <?php
                                $args = array(
	                                'post_type' => 'post',
	                                'posts_per_page' => 3,
                                );
                                $post_query = new WP_Query($args);
                                if ($post_query->have_posts()):
	                                while ($post_query->have_posts()):
		                                $post_query->the_post();
		                                global $post;
		                                ?>
                                        <div class="contet_discount-post">
                                            <div class="contet_post-images">
		                                        <?php if (has_post_thumbnail()) {
			                                        the_post_thumbnail();
		                                        } ?>
                                            </div>
                                            <div class="contet_post-title">
                                                <a href="<?php echo  get_permalink( get_the_ID()); ?>">
                                                    <h4>

				                                        <?php the_title() ?>

                                                    </h4>
                                                </a>
                                            </div>
                                        </div>

	                                <?php
	                                endwhile;
                                endif;
                                ?>
							</div>
						</div>
                        <div class="contet_mg-16 movie-new">
                            <div class="contet_discount-title">
                                <div class="contet_title-main">
                                    <img src="/wp-content/themes/silas/assets/images/vector_title.svg" alt="">
                                    <h1>Phim Mới</h1>
                                </div>
                                <a href="<?php echo get_permalink(get_page_by_path('silas-cinema')) ?>" class="contet_title-more">
                                    <h1>Xem Thêm</h1>
                                    <img src="/wp-content/themes/silas/assets/images/arrow-right.svg" alt="">
                                </a>
                            </div>
                            <div class="contet_relate-container row">
								<?php do_action('new_movie');  ?>
                            </div>
                        </div>
					</div>

				<?php endwhile; // End of the loop. ?>

			</main><!-- #main -->
		</div><!-- #primary -->


	</div><!--#content-inside -->
</div><!-- #content -->

<?php get_footer(); ?>
