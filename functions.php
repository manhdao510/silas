<?php
/**
 * Screenr functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Screenr
 */

/**
 * is Elementor editor?
 *
 * @return bool
 */
if ( ! function_exists( 'screenr_elementor_is_editor' ) ):
	function screenr_elementor_is_editor() {
		if ( class_exists( 'Elementor\Plugin' ) ) {
			if ( Elementor\Plugin::$instance->preview->is_preview_mode() || Elementor\Plugin::$instance->editor->is_edit_mode() ) {
				return true;
			}
		}

		return false;
	}
endif;

if ( ! function_exists( 'screenr_setup' ) ):
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function screenr_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Screenr, use a find and replace
		 * to change 'screenr' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'screenr', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_post_type_support( 'page', 'excerpt' );
		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'screenr-blog-grid-small', 350, 200, true );
		add_image_size( 'screenr-blog-grid', 540, 300, true );
		add_image_size( 'screenr-blog-list', 790, 400, true );
		add_image_size( 'screenr-service-small', 538, 280, true );

		add_theme_support(
			'custom-logo',
			array(
				'height'      => 60,
				'width'       => 240,
				'flex-height' => true,
				'flex-width'  => true,
				// 'header-text' => array( 'site-title', 'site-description' ),
			)
		);

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'screenr' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'screenr_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		add_theme_support(
			'custom-header',
			array(
				'default-image'          => get_template_directory_uri() . '/assets/images/header-default.jpg',
				'width'                  => 1600,
				'height'                 => 800,
				'flex-height'            => false,
				'flex-width'             => false,
				'uploads'                => true,
				'random-default'         => false,
				'header-text'            => false,
				'default-text-color'     => '',
				'wp-head-callback'       => '',
				'admin-head-callback'    => '',
				'admin-preview-callback' => '',
			)
		);

		// Recommend plugins.
		add_theme_support(
			'recommend-plugins',
			array(
				'contact-form-7' => array(
					'name'            => esc_html__( 'Contact Form 7', 'screenr' ),
					'active_filename' => 'contact-form-7/wp-contact-form-7.php',
				),
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * WooCommerce support.
		 */
		add_theme_support( 'woocommerce' );
		// Add support for WooCommerce.
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		/**
		 * Add support for Gutenberg.
		 *
		 * @link https://wordpress.org/gutenberg/handbook/reference/theme-support/
		 */
		add_theme_support( 'editor-styles' );
		add_theme_support( 'align-wide' );

		// Disables the block editor from managing widgets in the Gutenberg plugin.
		add_filter( 'gutenberg_use_widgets_block_editor', '__return_false' );
		// Disables the block editor from managing widgets.
		add_filter( 'use_widgets_block_editor', '__return_false' );
	}
endif;
add_action( 'after_setup_theme', 'screenr_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function screenr_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'screenr_content_width', 790 );
}

add_action( 'after_setup_theme', 'screenr_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function screenr_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'screenr' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	if ( class_exists( 'WooCommerce' ) ) {
		register_sidebar(
			array(
				'name'          => esc_html__( 'Shop', 'screenr' ),
				'id'            => 'sidebar-shop',
				'description'   => '',
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			)
		);
	}

	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 1', 'screenr' ),
			'id'            => 'footer-1',
			'description'   => screenr_sidebar_desc( 'footer-1' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 2', 'screenr' ),
			'id'            => 'footer-2',
			'description'   => screenr_sidebar_desc( 'footer-2' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 3', 'screenr' ),
			'id'            => 'footer-3',
			'description'   => screenr_sidebar_desc( 'footer-3' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer 4', 'screenr' ),
			'id'            => 'footer-4',
			'description'   => screenr_sidebar_desc( 'footer-4' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);

}

add_action( 'widgets_init', 'screenr_widgets_init' );

/**
 * Add Google Fonts, editor styles to WYSIWYG editor
 */
function screenr_editor_styles() {
	$font_url = screenr_fonts_url();
	if ( $font_url ) {
		add_editor_style( array( 'assets/css/editor-style.css', $font_url ) );
	}
}

add_action( 'after_setup_theme', 'screenr_editor_styles' );

/**
 * Enqueue scripts and styles.
 */
function screenr_scripts() {
	$theme   = wp_get_theme();
	$version = $theme->get( 'Version' );

	$font_url = screenr_fonts_url();
	if ( $font_url ) {
		wp_enqueue_style( 'screenr-fonts', $font_url, array(), null );
	}

	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', false,
		'4.0.0' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', false, '4.0.0' );
	wp_enqueue_style( 'screenr-style', get_template_directory_uri() . '/style.css' );


	wp_enqueue_script( 'screenr-plugin', get_template_directory_uri() . '/assets/js/plugins.js', array( 'jquery' ),
		'4.0.0', true );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(),
		'4.0.0', true );

	$screenr_js = array(
		'ajax_url'           => admin_url( 'admin-ajax.php' ),
		'full_screen_slider' => ( get_theme_mod( 'slider_fullscreen' ) ) ? true : false,
		'header_layout'      => get_theme_mod( 'header_layout' ),
		'slider_parallax'    => ( get_theme_mod( 'slider_parallax', 1 ) == 1 ) ? 1 : 0,
		'is_home_front_page' => ( is_page_template( 'template-frontpage.php' ) && is_front_page() ) ? 1 : 0,
		'autoplay'           => 7000,
		'speed'              => 700,
		'effect'             => 'slide',
		'gallery_enable'     => '',
	);

	// Load gallery scripts
	$galley_disable = get_theme_mod( 'gallery_disable' ) == 1 ? true : false;
	if ( ! $galley_disable || is_customize_preview() ) {
		$screenr_js['gallery_enable'] = 1;
		$display                      = get_theme_mod( 'gallery_display', 'grid' );
		if ( ! is_customize_preview() ) {
			switch ( $display ) {
				case 'masonry':
					wp_enqueue_script( 'screenr-gallery-masonry',
						get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array(), $version, true );
					break;
				case 'justified':
					wp_enqueue_script( 'screenr-gallery-justified',
						get_template_directory_uri() . '/assets/js/jquery.justifiedGallery.min.js', array(), $version,
						true );
					break;
				case 'slider':
				case 'carousel':
					wp_enqueue_script( 'screenr-gallery-carousel',
						get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), $version, true );
					break;
				default:
					break;
			}
		} else {
			wp_enqueue_script( 'screenr-gallery-masonry',
				get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array(), $version, true );
			wp_enqueue_script( 'screenr-gallery-justified',
				get_template_directory_uri() . '/assets/js/jquery.justifiedGallery.min.js', array(), $version, true );
			wp_enqueue_script( 'screenr-gallery-carousel',
				get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), $version, true );
		}
	}

	wp_enqueue_style( 'screenr-gallery-lightgallery', get_template_directory_uri() . '/assets/css/lightgallery.css' );

	wp_enqueue_script( 'screenr-theme', get_template_directory_uri() . '/assets/js/theme.js', array( 'jquery' ),
		'20120206', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_localize_script( 'screenr-theme', 'Screenr', apply_filters( 'screenr_localize_script', $screenr_js ) );
	wp_enqueue_script( 'js-custom', get_template_directory_uri() . '/assets/js/all.js', array( 'jquery' ), '4.0.0',
		true );
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/assets/css/all.css' );
	wp_enqueue_style( 'reset-css', get_template_directory_uri() . '/assets/css/reset.css' );
	if ( class_exists( 'WooCommerce' ) ) {
		wp_enqueue_style( 'screenr-woocommerce', get_template_directory_uri() . '/woocommerce.css' );
	}
	wp_register_style( 'workreap-responsive', get_template_directory_uri() . '/assets/css/responsive.css', array() );
	wp_enqueue_style( 'workreap-responsive' );

}

add_action( 'wp_enqueue_scripts', 'screenr_scripts' );

function the_breadcrumb() {
	echo '<ul id="crumbs">';
	if ( ! is_home() ) {
		echo '<li><img src="/wp-content/themes/silas/assets/images/Icon.svg"><a href="';
		echo get_option( 'home' );
		echo '">';
		echo 'Trang chủ';
		echo "</a></li>";
		if ( is_category() || is_single() ) {
			echo '<span>›</span><li> <img src="/wp-content/themes/silas/assets/images/grid.svg">';
			the_category( ' </li><li> <img src="/wp-content/themes/silas/assets/images/grid.svg">' );
			if ( is_single() ) {
				echo '</li><span>›</span><li> <img src="/wp-content/themes/silas/assets/images/iconoir_cinema-old.svg">';
				the_title();
				echo '</li>';
			}
		} elseif ( is_page() ) {
			echo '<span>›</span><li><img src="/wp-content/themes/silas/assets/images/grid.svg">';
			echo the_title();
			echo '</li>';
		}
	} elseif ( is_tag() ) {
		single_tag_title();
	} elseif ( is_day() ) {
		echo "<li>Archive for ";
		the_time( 'F jS, Y' );
		echo '</li>';
	} elseif ( is_month() ) {
		echo "<li>Archive for ";
		the_time( 'F, Y' );
		echo '</li>';
	} elseif ( is_year() ) {
		echo "<li>Archive for ";
		the_time( 'Y' );
		echo '</li>';
	} elseif ( is_author() ) {
		echo "<li>Author Archive";
		echo '</li>';
	} elseif ( isset( $_GET['paged'] ) && ! empty( $_GET['paged'] ) ) {
		echo "<li>Blog Archives";
		echo '</li>';
	} elseif ( is_search() ) {
		echo "<li>Search Results";
		echo '</li>';
	}
	echo '</ul>';
}

if ( ! function_exists( 'screenr_fonts_url' ) ):
	/**
	 * Register default Google fonts
	 */
	function screenr_fonts_url() {
		$fonts_url = '';

		/**
		 * @since 1.2.5 Check if google is disabled function then return false.
		 */
		$settings = false;
		if ( function_exists( 'Screenr\GoogleFonts\Downloader\get_download_settings' ) ) {
			$settings = Screenr\GoogleFonts\Downloader\get_download_settings();
		}

		if ( $settings && $settings['disable'] ) {
			return false;
		}

		/*
								  Translators: If there are characters in your language that are not
								* supported by Open Sans, translate this to 'off'. Do not translate
								* into your own language.
								*/
		$open_sans = _x( 'on', 'Open Sans font: on or off', 'screenr' );

		/*
								 Translators: If there are characters in your language that are not
								* supported by Montserrat, translate this to 'off'. Do not translate
								* into your own language.
								*/
		$montserrat = _x( 'on', 'Montserrat font: on or off', 'screenr' );

		if ( 'off' !== $montserrat || 'off' !== $open_sans ) {
			$font_families = array();

			if ( 'off' !== $open_sans ) {
				$font_families[] = 'Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic';
			}

			if ( 'off' !== $montserrat ) {
				$font_families[] = 'Montserrat:400,700';
			}

			$query_args = array(
				'family' => urlencode( implode( '|', $font_families ) ),
				'subset' => urlencode( 'latin,latin-ext' ),
			);

			$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
		}

		return esc_url_raw( $fonts_url );
	}
endif;

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Download google font to local.
 *
 * @since 1.2.5
 */
require get_template_directory() . '/inc/google-fonts-downloader/downloader.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Slider
 */
require get_template_directory() . '/inc/class-slider.php';

/**
 * Section navigation
 *
 * @since 1.1.9
 */
require get_template_directory() . '/inc/class-sections-navigation.php';

if ( class_exists( 'WooCommerce' ) ) {
	/**
	 * Woocommerce
	 */
	require get_template_directory() . '/inc/wc.php';

}

/**
 * Add theme info page
 */
require get_template_directory() . '/inc/dashboard.php';

/**
 * Add admin editor style
 */
require get_template_directory() . '/inc/admin/class-editor.php';

// require_once( trailingslashit( get_template_directory() ) . 'trt-customizer-pro/example-1/class-customize.php' );
require get_template_directory() . '/inc/show-film.php';

if ( ! function_exists( 'feature_promotion_home' ) ) {
	function feature_promotion_home() {
		$args       = array(
			'post_type'      => 'post',
			'posts_per_page' => 3,
		);
		$post_query = new WP_Query( $args );
		if ( $post_query->have_posts() ):
			while ( $post_query->have_posts() ):
				$post_query->the_post();
                global $post;
				$post_id = get_the_ID();

				$custom_field_value = get_post_meta( $post_id, 'promotion_image', true );
				$image_data         = wp_get_attachment_image_src( $custom_field_value, 'full' );
				$image_url          = ( $image_data ) ? $image_data[0] : '';
				$content = get_the_content();
				$trimmed_content = wp_trim_words($content, 15   );
				$title = get_the_title();
				$trimmed_content_title = wp_trim_words($title, 5);
				?>
                <div class="title-sale_box title-reactive col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                    <div class="img-sale">
                        <a href="<?php echo get_permalink( get_the_ID() ); ?>">
                        <img src="<?php echo $image_url ?>"
                             alt="<?php echo get_the_title(); ?>">
                        </a>
                    </div>
                    <div class="contet_promotion_box">

                        <div class="sale-title">
                            <a href="<?php echo get_permalink( get_the_ID() ); ?>"><?php echo $trimmed_content_title; ?></a></div>
                        <div class="sale-title-sub">
                            <p><?php echo $trimmed_content;?></p>
                        </div>
                        <a class="main-btn" href="<?php echo get_permalink(get_page_by_path('dat-phong')) ;?>">
                            <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21"
                                 fill="none">
                                <path
                                        fill-rule="evenodd"
                                        clip-rule="evenodd"
                                        d="M4.6214 1.69544C5.43073 1.25777 6.4062 1.25777 7.21553 1.69544C7.56748 1.88577 7.88241 2.20108 8.30334 2.62254C8.32759 2.64681 8.35218 2.67144 8.37716 2.69641L9.2996 3.61885C9.34261 3.66186 9.37603 3.69527 9.40641 3.72784C10.136 4.51005 10.2978 5.66532 9.81116 6.61787C9.79089 6.65755 9.76792 6.69889 9.73834 6.75212L9.73305 6.76164C9.68553 6.84718 9.66914 6.87684 9.65554 6.90322C9.30047 7.59178 9.35707 8.42054 9.80248 9.05441C9.81955 9.07871 9.83981 9.10586 9.89852 9.18414L9.99671 9.31505C10.0899 9.43937 10.1309 9.4939 10.1724 9.54689C10.5071 9.97462 10.8926 10.3601 11.3203 10.6949C11.3733 10.7363 11.4278 10.7773 11.5521 10.8705L11.6831 10.9687C11.7613 11.0274 11.7885 11.0477 11.8128 11.0647C12.4467 11.5101 13.2754 11.5667 13.964 11.2117C13.9903 11.1981 14.0201 11.1816 14.1056 11.1342L14.1153 11.1288C14.1684 11.0992 14.2097 11.0763 14.2493 11.0561C15.2019 10.5694 16.3571 10.7312 17.1394 11.4608C17.1719 11.4912 17.2053 11.5246 17.2483 11.5676L18.1708 12.4901C18.1958 12.515 18.2204 12.5396 18.2447 12.5639C18.6661 12.9848 18.9814 13.2997 19.1717 13.6517C19.6094 14.461 19.6094 15.4365 19.1717 16.2458C18.9814 16.5978 18.6661 16.9127 18.2447 17.3336C18.2204 17.3579 18.1958 17.3825 18.1708 17.4074C18.1488 17.4294 18.1271 17.4511 18.1057 17.4725C17.4669 18.1116 17.0537 18.525 16.5347 18.8192C15.4015 19.4618 13.8273 19.5477 12.6309 19.0322C12.0832 18.7962 11.6727 18.4665 11.0432 17.961C11.0242 17.9457 11.0049 17.9303 10.9855 17.9147C9.70246 16.8847 8.19479 15.5885 6.73672 14.1305C5.27866 12.6724 3.98249 11.1648 2.95255 9.88175C2.93693 9.86229 2.92148 9.84304 2.90618 9.82399C2.40068 9.19451 2.07104 8.78402 1.83503 8.2363C1.31952 7.03993 1.40539 5.46571 2.04796 4.33251C2.34224 3.81352 2.75559 3.40033 3.39467 2.76151C3.41612 2.74007 3.43782 2.71837 3.45978 2.69641C3.48475 2.67144 3.50935 2.64681 3.53359 2.62254C3.95452 2.20108 4.26945 1.88577 4.6214 1.69544Z"
                                        fill="#201F29"
                                />
                                <path
                                        fill-rule="evenodd"
                                        clip-rule="evenodd"
                                        d="M16.3577 4.56599C15.7175 3.92582 14.8399 3.41953 13.9119 3.36138C13.4917 3.33505 13.1297 3.65434 13.1034 4.07452C13.0771 4.49471 13.3964 4.85668 13.8166 4.88301C14.2707 4.91147 14.8184 5.18285 15.2796 5.64406C15.7408 6.10527 16.0122 6.65294 16.0406 7.10709C16.067 7.52727 16.4289 7.84656 16.8491 7.82023C17.2693 7.7939 17.5886 7.43193 17.5623 7.01174C17.5041 6.08379 16.9978 5.20616 16.3577 4.56599ZM14.3836 6.53308C13.9708 6.12019 13.3787 5.79987 12.7228 5.79987C12.3017 5.79987 11.9604 6.14117 11.9604 6.56218C11.9604 6.98319 12.3017 7.32449 12.7228 7.32449C12.8829 7.32449 13.1067 7.41232 13.3056 7.61115C13.5044 7.80998 13.5922 8.03379 13.5922 8.19397C13.5922 8.61498 13.9335 8.95628 14.3545 8.95628C14.7756 8.95628 15.117 8.61498 15.117 8.19397C15.117 7.53803 14.7965 6.94597 14.3836 6.53308Z"
                                        fill="#201F29"
                                        fill-opacity="0.48"
                                />
                            </svg>
                            Đặt phòng
                        </a>
                    </div>

                </div>
			<?php
			endwhile;
		endif;

	}

	add_action( 'feature_promotion_home', 'feature_promotion_home' );
}
if ( ! function_exists( 'feature_promotion' ) ) {
	function feature_promotion() {
		$args       = array(
			'post_type'      => 'post',
			'posts_per_page' => 3,
		);
		$post_query = new WP_Query( $args );
		if ( $post_query->have_posts() ):
			while ( $post_query->have_posts() ):
				$post_query->the_post();
				global $post;
				$content = get_the_title();
				$trimmed_content = wp_trim_words($content, 5);
				?>
                <div class="contet_relate-box col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                    <div class="contet_discount-post">
                        <div class="contet_post-images">
                            <a href="<?php echo get_permalink( get_the_ID() ); ?>">
							<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							} ?>
                            </a>
                        </div>

                        <div class="contet_post-title">
                            <a href="<?php echo get_permalink( get_the_ID() ); ?>">
                                <h4><?php echo $trimmed_content;?></h4>
                            </a>
                        </div>
                    </div>
                </div>
			<?php
			endwhile;
		endif;

	}

	add_action( 'feature_promotion', 'feature_promotion' );
}
if ( ! function_exists( 'new_movie' ) ) {
	function new_movie() {
		$args = array(
			'post_type'      => 'movie',
			'posts_per_page' => 3,
			'orderby'        => 'date',
			'order'          => 'DESC',
		);

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				global $post;
				$post_id            = $post->ID;
				$custom_field_value = get_post_meta( $post_id, 'image', true );
				$image_data         = wp_get_attachment_image_src( $custom_field_value );
				$image_url          = ( $image_data ) ? $image_data[0] : '';
				?>
                <div class="contet_movie-box col-12 col-sm-12 col-md-6 col-lg-4 float-left">
                    <a href="<?php echo get_permalink( get_the_ID() ); ?>">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} ?>
                    </a>
                    <span>
						<a href="<?php echo get_permalink( get_the_ID() ); ?>">
                                <h4>

									<?php the_title() ?>

                                </h4>
                            </a>
					</span>
                </div>
				<?php
			}

		}
	}

	add_action( 'new_movie', 'new_movie' );
}