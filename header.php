<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Screenr
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php
if ( function_exists( 'wp_body_open' ) ) {
	wp_body_open();
}
?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'screenr' ); ?></a>
	<?php
	$header_classes   = array();
	$header_classes[] = 'site-header';
	$header_layout    = get_theme_mod( 'header_layout' );
	if ( $header_layout == 'fixed' ) {
		$header_classes[] = 'sticky-header';
	} elseif ( $header_layout == 'transparent' ) {
		$header_classes[] = 'sticky-header';
		$header_classes[] = 'transparent';
	}
	$defaults = array(
		'theme_location'  => 'primary',
		'menu'            => '',
		'container'       => 'ul',
		'container_class' => '',
		'container_id'    => '',
		'echo'            => false,
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul id="%1$s" class="%2$s navbar-primary">%3$s</ul>',
	);
	?>
    <header id="masthead" class="<?php echo esc_attr( join( ' ', $header_classes ) ); ?>" role="banner">
        <section class="header wt-sticky">
			<?php if ( function_exists( 'has_custom_logo' ) && has_custom_logo() ) { ?>
            <strong class="logo_home">
				<?php the_custom_logo();
				} ?>
            </strong>
            <div class="wt-rightarea">
                <nav id="wt-nav" class="wt-nav navbar-expand-lg">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNav" aria-controls="navbarNav"
                            aria-expanded="false" aria-label="Toggle navigation"><span> Menu </span>
                        <img src="/wp-content/themes/silas/assets/images/bars-right.svg">
                    </button>
                    <div class="collapse navbar-collapse wt-navigation" id="navbarNav">
						<?php echo do_shortcode( wp_nav_menu( $defaults ) ); ?>
                    </div>
                </nav>
            </div>
            <div class="search_bar">
                <div class="search_bar-icon" id="search_bar-icon">
                    <img class="search_bar_icon_image" src="/wp-content/themes/silas/assets/images/search-bars.png"
                         alt="">
                </div>
                <form class="search-form" id="search_form" action="<?php echo get_permalink(get_page_by_path('silas-cinema')) ?>" method ="get">
                    <input type="search" placeholder="Chỉ để tìm kiếm phim" class="input_search-bar" name="film">
                    <button aria-label="Search Resources" type="submit" class="search_btn">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512">
                            <!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. -->
                            <style>svg {
                                    fill: #ffffff
                                }</style>
                            <path d="M416 208c0 45.9-14.9 88.3-40 122.7L502.6 457.4c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L330.7 376c-34.4 25.2-76.8 40-122.7 40C93.1 416 0 322.9 0 208S93.1 0 208 0S416 93.1 416 208zM208 352a144 144 0 1 0 0-288 144 144 0 1 0 0 288z"/>
                        </svg>
                        Search
                    </button>
                </form>
            </div>
        </section>
        <section class="slider_bar">
            <div class="slider">
                <div class="slides">
                    <div class="slide"><img src="/wp-content/themes/silas/assets/images/bg-x1.png"
                                            alt="Slide 1"/></div>
                    <div class="slide"><img src="/wp-content/themes/silas/assets/images/bg-x2%20.png" alt="Slide 2"/>
                    </div>
                    <div class="slide"><img src="/wp-content/themes/silas/assets/images/bg-x3.png" alt="Slide 3"/>
                    </div>
                    <div class="slide"><img src="/wp-content/themes/silas/assets/images/bg-x4.png" alt="Slide 4"/>
                    </div>
                </div>
                <button class="nextBtn">
                    <img src="/wp-content/themes/silas/images/slider_bar/arrow-drop-right-line.png" alt="Slide 4"/>
                </button>

                <button class="prevBtn">
                    <img src="/wp-content/themes/silas/images/slider_bar/arrow-drop-left-line.png" alt="Slide 4"/>
                </button>
            </div>
        </section>
    </header><!-- #masthead -->

<?php

